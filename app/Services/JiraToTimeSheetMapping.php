<?php

namespace App\Services;

use App\Models\ProjectMapping;
use Illuminate\Database\Eloquent\Collection;
use JiraRestApi\Configuration\ArrayConfiguration;
use JiraRestApi\Issue\IssueSearchResult;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\Issue\PaginatedWorklog;
use JiraRestApi\JiraException;
use JsonMapper_Exception;

class JiraToTimeSheetMapping
{

    /**
     * @var IssueService
     */
    private $issueService;

    /**
     * @throws JsonMapper_Exception
     */
    public function process()
    {
        try {
            $projectMappings = $this->getAllProjectMappings();
            foreach ($projectMappings as $projectMapping) {
                if ($this->checkIfUserIsAssignedToProject($projectMapping)) {
                    $mapping = [];
                    $userIssues = $this->resolveUserIssues($projectMapping);
                    foreach ($userIssues->issues as $key => $userIssue) {
                        $issueWorklog = $this->resolveIssueWorklog($userIssue->id);
                        if ($issueWorklog->worklogs) {
                            foreach ($issueWorklog->worklogs as $worklog) {
                                $mapping[] = [
                                    'project_mapping_id' => $projectMapping->id,
                                    'jira_issue_id' => $userIssue->id,
                                    'jira_worklog_id' => $worklog->id,
                                    'timesheet_entity_id' => null
                                ];
                            }

                            $this->insertJiraToTimeSheetMapping($mapping);
                        }
                    }

                }
            }
        } catch (JiraException $e) {
            print("Error Occured: " . $e->getMessage());
        }
    }

    /**
     * @param $mapping
     */
    protected function insertJiraToTimeSheetMapping($mapping)
    {
        \App\Models\JiraToTimeSheetMapping::insert($mapping);
    }

    /**
     * @return ProjectMapping[]|Collection
     */
    protected function getAllProjectMappings()
    {
        return ProjectMapping::all();
    }

    /**
     * @param $projectMapping
     * @return IssueSearchResult
     * @throws JiraException
     * @throws JsonMapper_Exception
     */
    protected function resolveUserIssues($projectMapping)
    {
        $arrayConfiguration = new ArrayConfiguration([
            'jiraHost' => $projectMapping->jira_endpoint,
            'jiraUser' => $projectMapping->jira_user,
            'jiraPassword' => $projectMapping->jira_api_token,
        ]);
        $this->issueService = new IssueService($arrayConfiguration);

        $projectIssues = $this->issueService->search('project=' . $projectMapping->jira_project_id);
        return $projectIssues;
    }

    /**
     * @param $projectIssueId
     * @return PaginatedWorklog
     * @throws JiraException
     * @throws JsonMapper_Exception
     */
    protected function resolveIssueWorklog($projectIssueId)
    {
        $issueWorklog = $this->issueService->getWorklog($projectIssueId);
        return $issueWorklog;
    }

    /**
     * @param $projectMapping
     * @return mixed
     */
    protected function checkIfUserIsAssignedToProject($projectMapping)
    {
        $checkAssign = \App\Models\PeopleMapping::where('timesheet_project_id', $projectMapping->timesheet_project_id)
            ->where('jira_project_id', $projectMapping->jira_project_id)
            ->where('jira_user', $projectMapping->jira_user)
            ->first();

        return $checkAssign;
    }

}