<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JiraToTimeSheetMapping extends Model
{

    protected $table = 'jira_to_timesheet_mapping';
    protected $fillable = ['project_mapping_id', 'jira_issue_id', 'jira_worklog_id', 'timesheet_entity_id'];
    public $with = ['projectMapping'];

    const CREATED_AT = false;
    const UPDATED_AT = false;

    public function projectMapping()
    {
        return $this->hasOne(ProjectMapping::class, 'id', 'project_mapping_id');
    }

}