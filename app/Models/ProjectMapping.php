<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectMapping extends Model
{

    protected $table = 'project_mapping';
    protected $fillable = ['timesheet_project_id', 'jira_project_id', 'jira_endpoint', 'jira_user', 'jira_api_token'];

    const CREATED_AT = false;
    const UPDATED_AT = false;

}