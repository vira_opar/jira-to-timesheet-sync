<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeopleMapping extends Model
{

    protected $table = 'people_mapping';
    protected $fillable = ['timesheet_user_id', 'timesheet_project_id', 'jira_project_id', 'jira_user'];

    const CREATED_AT = false;
    const UPDATED_AT = false;

}