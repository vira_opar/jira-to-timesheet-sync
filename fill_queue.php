<?php

require_once(__DIR__ . '/bootstrap.php');

$mapping = new \App\Services\JiraToTimeSheetMapping();
$mapping->process();
